FROM centos:7

MAINTAINER kolinz <knishikawa@kolinzlabs.com>

# yum update
RUN yum update -y && \
yum install epel-release -y && \
yum install wget nano curl git -y && \
yum upgrade -y && \
yum clean all

# Set Localtime
RUN ln -sf /usr/share/zoneinfo/Asia/Tokyo /etc/localtime && \
ls -l /etc/localtime

# Create response.varfile for JIRA Software
RUN touch response.varfile && \
echo launch.application$Boolean=true > response.varfile && \
echo rmiPort$Long=8005 >> response.varfile && \
echo app.jiraHome=/var/atlassian/application-data/jira >> response.varfile && \
echo app.install.service$Boolean=true >> response.varfile && \
echo existingInstallationDir=/opt/JIRA Software >> response.varfile && \
echo sys.confirmedUpdateInstallationString=false >> response.varfile && \
echo sys.languageId=en >> response.varfile && \
echo sys.installationDir=/opt/atlassian/jira >> response.varfile && \
echo executeLauncherAction$Boolean=true >> response.varfile && \
echo httpPort$Long=8080 >> response.varfile && \
echo portChoice=default >> response.varfile

# Auto Install JIRA Software 7.10.0
RUN wget http://www.atlassian.com/software/jira/downloads/binary/atlassian-jira-software-7.10.0-x64.bin && \
chmod a+x atlassian-jira-software-7.10.0-x64.bin && \
./atlassian-jira-software-7.10.0-x64.bin -q -varfile response.varfile && \
rm -f atlassian-jira-software-7.10.0-x64.bin

# Install Japanese Fonts
RUN yum install -y unzip nano wget && \
yum install -y ibus-kkc ipa-gothic-fonts ipa-pgothic-fonts vlgothic-* && \
wget https://oscdl.ipa.go.jp/IPAexfont/IPAexfont00301.zip && \
wget https://oscdl.ipa.go.jp/IPAfont/IPAfont00303.zip && \
unzip IPAexfont00301.zip && \
unzip IPAfont00303.zip && \
mkdir -p /opt/atlassian/jira/jre/lib/fonts/fallback && \
cp -a IPAexfont00301/*.ttf /opt/atlassian/jira/jre/lib/fonts/fallback/ && \
cp -a IPAfont00303/*.ttf /opt/atlassian/jira/jre/lib/fonts/fallback/ && \
rm -rf IPAexfont00301 && \
rm -rf IPAfont00303 && \
rm -f IPAexfont00301.zip && \
rm -f IPAfont00303.zip

# Change Context path
RUN sed -i 's|<Context path=""|<Context path="/jira"|' /opt/atlassian/jira/conf/server.xml

# Change Permission
RUN chown -R jira:jira /opt/atlassian && \
chmod 755 /opt/atlassian/jira/bin/*.sh

# Create Start Script
RUN touch start.sh && \
echo \#\!/bin/sh >> start.sh && \
echo chown -R jira:jira /var/atlassian/application-data/jira >> start.sh && \
echo /opt/atlassian/jira/bin/start-jira.sh >> start.sh && \
echo /bin/bash >> start.sh
RUN chmod a+x start.sh

# Define working directory
WORKDIR /opt/atlassian/jira

# Define default command
CMD ["start.sh"]