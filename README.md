# jira-docker
CentOS 7.x ベースのJira Software用Dockerfile。コンテナ内のタイムゾーンを日本時間に変更、日本語フォントを導入します。
取得したDockerfileをビルドしてください。

$ docker build -t jira:710 .


# Dockerで実行中のコンテナからサーバー内で実行するローカルのデータベースに接続する場合
ホスト側のIPアドレスを調べます。

$ sudo yum install net-tools
$ ifconfig | grep -E "([0-9]{1,3}\.){3}[0-9]{1,3}" | grep -v 127.0.0.1 | awk '{ print $2 }' | cut -f2 -d: | head -n1



ホスト側のIPアドレスが表示されます。表示されたIPアドレスを、docker run コマンド実行時に、--add-host に追加します。

$ docker run -itd -p 8080:8080 --add-host=localhost:xxx.xxx.xxx.xxx --name jira-server -v /var/atlassian/application-data/jira:/var/atlassian/application-data/jira jira:710 /start.sh

Jiraのセットアップ画面で、データベース接続のホストに、ホスト側のIPアドレスを入力して接続テストを実施します。


# リバースプロキシを用いてJiraに、https接続するための設定

リバースプロキシサーバを立て、SSL接続を可能にする。Let's Encryptがお金がかからず便利。  　
稼働中のJira用コンテナに、docker attach コンテナID でアクセス。  


# sed -i 's|bindOnInit="false"/>|bindOnInit="false" scheme="https" proxyPort="443" URIEncoding="UTF-8"/>|' /opt/atlassian/jira/conf/server.xml
# cd /opt/atlassian/jira/bin/
# ./stop-jira.sh
# ./start-jira.sh


Ctrl+p+qキーを押して、コンテナから離脱。
